package com.ra655566.ad.auction;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.ra655566.ad.auction.model.Banner;

public class AdAuctionTest {

    private static final AdAuction adAuction = new AdAuction();

    @Test
    public void selectBanners_nullBanners() {
        Exception e = Assertions.assertThrows(
            NullPointerException.class,
            () -> adAuction.selectBanners(null, 1, "RU")
        );

        Assertions.assertEquals(
            "banners are required",
            e.getMessage()
        );
    }

    @Test
    public void selectBanners_countLessThan0() {
        Banner[] banners = new Banner[]{
            new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")))
        };

        Exception e = Assertions.assertThrows(
            IllegalArgumentException.class,
            () -> adAuction.selectBanners(banners, -1, "RU")
        );

        Assertions.assertEquals(
            "count can't be less than 0",
            e.getMessage()
        );
    }

    @Test
    public void selectBanners_emptyBanners() {
        Banner[] banners = new Banner[]{};

        List<Banner> expected = Collections.emptyList();
        List<Banner> actual = adAuction.selectBanners(banners, 1, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_blankCountryCode() {
        Banner[] banners = new Banner[]{
            new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")))
        };

        Exception e = Assertions.assertThrows(
            IllegalArgumentException.class,
            () -> adAuction.selectBanners(banners, 1, null)
        );

        Assertions.assertEquals(
            "countryCode is required",
            e.getMessage()
        );
    }

    @Test
    public void selectBanners_sameCampaign_countLessThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId, 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Collections.singletonList(
            banner3
        );
        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_sameCampaign_countryFilter_countLessThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId, 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", campaignId, 4L, new HashSet<>(Arrays.asList("US", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4
        };

        List<Banner> expected = Collections.singletonList(
            banner3
        );
        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_sameCampaign_countMoreThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId, 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Collections.singletonList(
            banner3
        );
        List<Banner> actual = adAuction.selectBanners(banners, 4, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_sameCampaign_countryFilter_countEqToBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId, 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", campaignId, 4L, new HashSet<>(Arrays.asList("US", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Collections.singletonList(
            banner3
        );
        List<Banner> actual = adAuction.selectBanners(banners, 4, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_allCampaignsDifferent_countLessThanBannersLength() {
        Banner banner1 = new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", "2", 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Arrays.asList(
            banner3,
            banner2
        );
        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_allCampaignsDifferent_countMoreThanBannersLength() {
        Banner banner1 = new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", "2", 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Arrays.asList(
            banner3,
            banner2,
            banner1
        );
        List<Banner> actual = adAuction.selectBanners(banners, 4, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_CampaignsDifferent_countLessThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Arrays.asList(
            banner3,
            banner2
        );
        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_campaignsDifferent_countMoreThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3
        };

        List<Banner> expected = Arrays.asList(
            banner3,
            banner2
        );
        List<Banner> actual = adAuction.selectBanners(banners, 4, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_campaignsDifferent_countryFilter_countLessThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", "4", 4L, new HashSet<>(Arrays.asList("US", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4
        };

        List<Banner> expected = Arrays.asList(
            banner3,
            banner2
        );
        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_campaignsDifferent_countryFilter_countMoreThanBannersLength() {
        String campaignId = "1";
        Banner banner1 = new Banner("1", campaignId, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", "4", 4L, new HashSet<>(Arrays.asList("US", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4
        };

        List<Banner> expected = Arrays.asList(
            banner3,
            banner2
        );
        List<Banner> actual = adAuction.selectBanners(banners, 5, "RU");

        Assertions.assertIterableEquals(expected, actual);
    }

    @Test
    public void selectBanners_samePrice_count2() {
        String campaignId1 = "1";
        String campaignId2 = "2";
        String campaignId3 = "3";
        Banner banner1 = new Banner("1", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", campaignId3, 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5
        };

        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertEquals(2, actual.size());
        Assertions.assertTrue(actual.contains(banner5));
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId2.equals(b.getCampaignId()))
        );
    }

    @Test
    public void selectBanners_samePrice_count3() {
        String campaignId1 = "1";
        String campaignId2 = "2";
        String campaignId3 = "3";
        Banner banner1 = new Banner("1", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", campaignId3, 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5
        };

        List<Banner> actual = adAuction.selectBanners(banners, 3, "RU");

        Assertions.assertEquals(3, actual.size());
        Assertions.assertTrue(actual.contains(banner5));
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId2.equals(b.getCampaignId()))
        );
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId1.equals(b.getCampaignId()))
        );
    }

    @Test
    public void selectBanners_samePrice_count2_countryFilter() {
        String campaignId1 = "1";
        String campaignId2 = "2";
        String campaignId3 = "3";
        Banner banner1 = new Banner("1", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", campaignId3, 3L, new HashSet<>(Arrays.asList("US", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5
        };

        List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

        Assertions.assertEquals(2, actual.size());
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId2.equals(b.getCampaignId()))
        );
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId1.equals(b.getCampaignId()))
        );
    }

    @Test
    public void selectBanners_samePrice_count3_countryFilter() {
        String campaignId1 = "1";
        String campaignId2 = "2";
        String campaignId3 = "3";
        Banner banner1 = new Banner("1", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", campaignId1, 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", campaignId2, 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", campaignId3, 3L, new HashSet<>(Arrays.asList("US", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5
        };

        List<Banner> actual = adAuction.selectBanners(banners, 3, "RU");

        Assertions.assertEquals(2, actual.size());
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId2.equals(b.getCampaignId()))
        );
        Assertions.assertTrue(
            actual.stream()
                .anyMatch(b -> campaignId1.equals(b.getCampaignId()))
        );
    }

    @Test
    public void selectBanners_equalLikelihood() {
        Banner banner1 = new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "1", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", "2", 2L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner6 = new Banner("6", "3", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner7 = new Banner("7", "4", 4L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner8 = new Banner("8", "5", 3L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5,
            banner6,
            banner7,
            banner8
        };

        /*
        Having the banners array represented above, we will attempt to select only 2 banners.

        As a result we must get 1 of 3 possible options:
        - banner7, banner3
        - banner7, banner6
        - banner7, banner8

        So we will do several iterations and expect that all 3 options will occur with approximately equal likelihood.
         */
        Map<String, Integer> map = new HashMap<>();

        int iterations = 1_000_000;

        for (int i = 0; i < iterations; i++) {
            List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

            Assertions.assertEquals(2, actual.size());

            // Count occurrences of each option
            map.merge(
                actual.get(0).getId() + "_" + actual.get(1).getId(),
                1,
                Integer::sum
            );
        }

        double medium = iterations / 3D;
        double delta = medium * 0.01; // assume having a delta about 1%
        int lowerBound = (int) (medium - delta);
        int upperBound = (int) (medium + delta);

        map.values()
            .forEach(value -> Assertions.assertTrue(
                lowerBound <= value && value <= upperBound,
                String.format(
                    "lowerBound = %s, value = %s, upperBound = %s",
                    lowerBound,
                    value,
                    upperBound
                )
            ));
    }

    @Test
    public void selectBanners_equalLikelihood_onlyOneCampaign() {
        Banner banner1 = new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4
        };

        /*
        Having the banners array represented above, we will attempt to select only 1 banner.

        As a result we must get 1 of 4 possible options:
        - banner1
        - banner2
        - banner3
        - banner4

        So we will do several iterations and expect that all 4 options will occur with approximately equal likelihood.
         */

        Map<String, Integer> map = new HashMap<>();

        int iterations = 1_000_000;

        for (int i = 0; i < iterations; i++) {
            List<Banner> actual = adAuction.selectBanners(banners, 1, "RU");

            Assertions.assertEquals(1, actual.size());

            // Count occurrences of each option
            map.merge(
                actual.get(0).getId(),
                1,
                Integer::sum
            );
        }

        double medium = iterations / 4D;
        double delta = medium * 0.01; // assume having a delta about 1%
        int lowerBound = (int) (medium - delta);
        int upperBound = (int) (medium + delta);

        map.values()
            .forEach(value -> Assertions.assertTrue(
                lowerBound <= value && value <= upperBound,
                String.format(
                    "lowerBound = %s, value = %s, upperBound = %s",
                    lowerBound,
                    value,
                    upperBound
                )
            ));
    }

    @Test
    public void selectBanners_equalLikelihood_twoCampaigns_1() {
        Banner banner1 = new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", "2", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", "2", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5
        };

        Map<String, Integer> map = new HashMap<>();

        int iterations = 1_000_000;

        for (int i = 0; i < iterations; i++) {
            List<Banner> actual = adAuction.selectBanners(banners, 1, "RU");

            Assertions.assertEquals(1, actual.size());

            // Count occurrences of each option
            map.merge(
                actual.get(0).getId(),
                1,
                Integer::sum
            );
        }

        double medium = iterations / 5D;
        double delta = medium * 0.01; // assume having a delta about 1%
        int lowerBound = (int) (medium - delta);
        int upperBound = (int) (medium + delta);

        map.values()
            .forEach(value -> Assertions.assertTrue(
                lowerBound <= value && value <= upperBound,
                String.format(
                    "lowerBound = %s, value = %s, upperBound = %s",
                    lowerBound,
                    value,
                    upperBound
                )
            ));
    }

    @Test
    public void selectBanners_equalLikelihood_twoCampaigns_2() {
        Banner banner1 = new Banner("1", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner2 = new Banner("2", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner3 = new Banner("3", "1", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner4 = new Banner("4", "2", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));
        Banner banner5 = new Banner("5", "2", 1L, new HashSet<>(Arrays.asList("RU", "US", "CN")));

        Banner[] banners = new Banner[]{
            banner1,
            banner2,
            banner3,
            banner4,
            banner5
        };

        Map<String, Integer> map = new HashMap<>();

        int iterations = 1_000_000;

        for (int i = 0; i < iterations; i++) {
            List<Banner> actual = adAuction.selectBanners(banners, 2, "RU");

            Assertions.assertEquals(2, actual.size());

            // Count occurrences of each option
            map.merge(
                actual.get(0).getId() + "_" + actual.get(1).getId(),
                1,
                Integer::sum
            );
        }

        double medium = iterations / 12D;
        double delta = medium * 0.01; // assume having a delta about 1%
        int lowerBound = (int) (medium - delta);
        int upperBound = (int) (medium + delta);

        map.values()
            .forEach(value -> Assertions.assertTrue(
                lowerBound <= value && value <= upperBound,
                String.format(
                    "lowerBound = %s, value = %s, upperBound = %s",
                    lowerBound,
                    value,
                    upperBound
                )
            ));
    }
}
