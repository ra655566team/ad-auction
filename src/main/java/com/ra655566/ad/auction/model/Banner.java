package com.ra655566.ad.auction.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;

public class Banner {

    private final String id;
    private final String campaignId;
    private final Long price; // in USD cents
    private final HashSet<String> countryCodes;

    public Banner(
        String id,
        String campaignId,
        Long price,
        HashSet<String> countryCodes
    ) {
        this.id = Objects.requireNonNull(id, "id is required");
        this.campaignId = Objects.requireNonNull(campaignId, "campaignId is required");
        this.price = Objects.requireNonNull(price, "price is required");
        this.countryCodes = Optional.ofNullable(countryCodes).orElse(new HashSet<>());
    }

    public String getId() {
        return id;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public Long getPrice() {
        return price;
    }

    public HashSet<String> getCountryCodes() {
        return countryCodes;
    }

    @Override
    public String toString() {
        return "Banner{" +
               "id='" + id + '\'' +
               ", campaignId='" + campaignId + '\'' +
               ", price=" + price +
               ", countryCodes=" + countryCodes +
               '}';
    }
}
