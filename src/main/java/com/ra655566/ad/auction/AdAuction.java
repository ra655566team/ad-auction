package com.ra655566.ad.auction;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ra655566.ad.auction.model.Banner;

/**
 * Implementation of ad auction.
 */
public class AdAuction {

    public List<Banner> selectBanners(
        Banner[] banners,
        int count,
        String countryCode
    ) {
        return selectBanners(
            banners,
            count,
            countryCode,
            null,
            null
        );
    }

    public List<Banner> selectBanners(
        Banner[] banners,
        int count,
        String countryCode,
        Predicate<Banner> additionalFilter,
        Comparator<Banner> additionalPriority
    ) {
        // validate parameters
        Objects.requireNonNull(banners, "banners are required");
        if (count < 0) {
            throw new IllegalArgumentException("count can't be less than 0");
        }
        if (banners.length == 0 || count == 0) {
            return Collections.emptyList();
        }
        if (countryCode == null || countryCode.trim().isEmpty()) {
            throw new IllegalArgumentException("countryCode is required");
        }

        // compose filter by country with additional filters
        Predicate<Banner> countryFilter = banner -> banner.getCountryCodes().isEmpty() ||
                                                    banner.getCountryCodes().contains(countryCode);
        Predicate<Banner> compositeFilter = Optional.ofNullable(additionalFilter)
            .map(countryFilter::and)
            .orElse(countryFilter);

        // compose priority comparator with additional priority comparators
        Comparator<Banner> pricePriority = Comparator.comparing(Banner::getPrice).reversed();
        Comparator<Banner> compositeComparator = Optional.ofNullable(additionalPriority)
            .map(pricePriority::thenComparing)
            .orElse(pricePriority);

        // filter and sort
        List<Banner> bannerList = Stream.of(banners)
            .filter(compositeFilter)
            .collect(Collectors.toList());

        // shuffle elements to ensure that equal elements will occur with approximately equal likelihood
        Collections.shuffle(bannerList);

        return bannerList.stream()
            .sorted(compositeComparator)
            .filter(distinctByKey(Banner::getCampaignId))
            .limit(count)
            .collect(Collectors.toList());
    }

    private <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
